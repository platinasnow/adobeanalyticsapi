package org.adobe.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class JsonParamService {

	String[] metrics = {"pageviews", "visits"};
	
	public String buildJsonParam(String rsid, String date, String[] keyword) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("rsid", rsid);
		jsonObject.put("globalFilters", this.getGlobalFilter(date));
		jsonObject.put("search", this.getSearchClause(keyword));
		
		JSONObject metricContainer = new JSONObject();
		metricContainer.put("metrics", this.getMetrics());
		metricContainer.put("metricFilters", this.getMetricFilters(date));
		jsonObject.put("metricContainer", metricContainer);
		
		jsonObject.put("dimension", "variables/page");
		jsonObject.put("settings", this.getSetting());
		
		System.out.println(jsonObject.toJSONString());
		return jsonObject.toJSONString();
	}
	
	
	private JSONObject getSearchClause(String[] keyword) {
		JSONObject searchClause = new JSONObject();
		StringBuilder keywordBuilder = new StringBuilder();
		for(int i=0, size = keyword.length; i < size ; i++ ) {
			keywordBuilder.append("(CONTAINS '");
			keywordBuilder.append(keyword[i]);
			keywordBuilder.append("')");
			if(size > i + 1 ) {
				keywordBuilder.append(" OR ");	
			}
		}
		
		searchClause.put("clause", keywordBuilder.toString());
		return searchClause;
	}
	
	private JSONArray getGlobalFilter(String date) {
		JSONArray globalFiltersArray = new JSONArray();
		JSONObject globalFiltersObject = new JSONObject();
		globalFiltersObject.put("type", "dateRange");
		globalFiltersObject.put("dateRange", this.getDailyDate(date));
		globalFiltersArray.add(globalFiltersObject);
		return globalFiltersArray; 
	}
	
	private JSONArray getMetricFilters(String date) {
		JSONArray metricFilters = new JSONArray();
		JSONObject metricFiltersObject = new JSONObject();
		metricFiltersObject.put("id", "0");
		metricFiltersObject.put("type", "dateRange");
		metricFiltersObject.put("dateRange", this.getDailyDate(date));
		metricFilters.add(metricFiltersObject);
		return metricFilters;
	}
	
	private String getDailyDate(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(sdf.parse(date));
			cal.add(Calendar.DATE, 1);
			String tomorrow = sdf.format(cal.getTime());
			return date+ "T00:00:00.000/"+ tomorrow+"T00:00:00.000";
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private JSONArray getMetrics() {
		JSONArray metricsArray = new JSONArray();
		for(int i=0, size = metrics.length; i < size; i ++) {
			metricsArray.add(this.getMetricColumn(i, metrics[i]));
		}
		return metricsArray;
	}
	
	private JSONObject getMetricColumn(int idx, String metrics) {
		JSONObject metricsColumn = new JSONObject();
		metricsColumn.put("columnId", "\""+idx+"\"");
		metricsColumn.put("id", "metrics/"+metrics);
		JSONArray metricsColumnFilter = new JSONArray();		
		metricsColumnFilter.add("0");
		metricsColumn.put("filters", metricsColumnFilter);
		return metricsColumn;
	}
	
	private JSONObject getSetting() {
		JSONObject setting = new JSONObject();
		setting.put("dimensionSort", "asc");
		setting.put("limit", "0");
		setting.put("page", "0");
		return setting;
	}
	
}
