package org.adobe.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

public class HttpPostService {

	
	public HttpPost buildHttpPost(String reqUrl, String paramStr) {
		HttpPost post = new HttpPost(reqUrl);
		post.addHeader("Authorization", "Bearer eyJ4NXUiOiJpbXNfbmExLWtleS0xLmNlciIsImFsZyI6IlJTMjU2In0.eyJpZCI6IjE1NDQ1OTU3MzEwNjJfYjdlZDNlOGMtMDUxOC00YWUyLTlkNTgtNDE4MGNiMjA4YTIyX3VlMSIsImNsaWVudF9pZCI6IjVhOGRjYzJjZmE3MTQ3MmNiZmE0ZmI1MzY3MWM0NWVkIiwidXNlcl9pZCI6IjUyRTY1OUFENTU5REQ3RDQ3RjAwMDEwMUBBZG9iZUlEIiwic3RhdGUiOiIiLCJ0eXBlIjoiYWNjZXNzX3Rva2VuIiwiYXMiOiJpbXMtbmExIiwiZmciOiJUQUhINVJVUlhMUDM3SEdHMjRGUUFBQUFZVT09PT09PSIsInNpZCI6IjE1NDQ1OTU3MzAwNjlfMzE0YTE5MmQtYTYyZC00OGMwLWJlNjEtMzU0ZGQ5ZmIyYThkX3VlMSIsInJ0aWQiOiIxNTQ0NTk1NzMxMDY0X2NiZDExMTJiLWU0N2QtNDRjYS04ZDMyLTNkY2RjNzE5NDA4Ml91ZTEiLCJvYyI6InJlbmdhKm5hMXIqMTY3YTExNDZkMTEqSkRORUozSzFLUzQ0VjQ2VEc2NFQzWkVHRjQiLCJydGVhIjoiMTU0NTgwNTMzMTA2NCIsIm1vaSI6ImE0ZGJkYjQ4IiwiYyI6IldqclFqV2dPWGYxczN2S1QrbklxWXc9PSIsImV4cGlyZXNfaW4iOiI4NjQwMDAwMCIsInNjb3BlIjoib3BlbmlkLEFkb2JlSUQscmVhZF9vcmdhbml6YXRpb25zLGFkZGl0aW9uYWxfaW5mby5wcm9qZWN0ZWRQcm9kdWN0Q29udGV4dCxhZGRpdGlvbmFsX2luZm8uam9iX2Z1bmN0aW9uIiwiY3JlYXRlZF9hdCI6IjE1NDQ1OTU3MzEwNjIifQ.D3JRmqAI6sVfeY9PnAcrCrjWB2K3R9u_A4H7j2nB7VmAVT9_bM3Q19L8IN3Psw0dsVsL7lbGujLs_1cQ-qM6DdpjpQ3GYVnSCLM2DpZUyOLto-ofNjmQIEu2f1epAn0indJ-iiOpM8g--9oJaZukrC6U07-2lTTJ-tQ_uudQ1cor2HXGGPso9fQyGsqtFfUO1rhVEpxP_pVEaEWIG1-P5jqZ4NgsxUjdh77GmrHsVQ5W7n1Qz1pQK9J6QxejYFRIr0xr7uVYAw8E8GESvwM77HGrNe8zQiptImAH-vLh1S6blKyjibUmB0IfMrBWJr5Kd3w8M206lXdUEtZFB4r73g");
		post.addHeader("x-api-key", "5a8dcc2cfa71472cbfa4fb53671c45ed");
		post.addHeader("x-proxy-global-company-id", "gmap1");
		post.addHeader("Accept", "application/json");
		post.addHeader("Content-Type", "application/json");
		
		try {
			StringEntity params =new StringEntity(paramStr);
			post.setEntity(params);
		} catch (UnsupportedEncodingException e) {
			System.out.println(e.getMessage());
		}
		return post;
	}
	
	public <T> T executePost(HttpPost post, Class<T> clazz) {
		T result = null;
		try {
			HttpClient client = HttpClientBuilder.create().build();
			
			HttpResponse response = client.execute(post);
			BufferedReader rd = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));
	
			StringBuffer resultBuffer = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				resultBuffer.append(line);
			}
			System.out.println("response : " + resultBuffer.toString());
			ObjectMapper mapper = new ObjectMapper();
			result = mapper.readValue(resultBuffer.toString(), clazz);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		return result;
	}
}
