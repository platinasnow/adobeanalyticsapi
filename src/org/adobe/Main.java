package org.adobe;

import org.adobe.model.ResponseReport;
import org.adobe.service.HttpPostService;
import org.adobe.service.JsonParamService;
import org.apache.http.client.methods.HttpPost;

public class Main {

	public static void main(String[] args) {
		
		//TODO OAUTH ���
		//TODO JSON ����
		String postUrl = "https://analytics.adobe.io/api/gmap1/reports";
		String[] chevoletReportSuiteIDs = { "gmapkrchevroletbb", "gmapkrchevroletmobile" };
		String[] chevoletKeywords = { 
				"ch:ich:kr:ko:cars:vehicle:spark",
				"ch:ich:kr:ko:cars:ev:volt",
				"ch:ich:kr:ko:cars:ev:boltev",
				"ch:ich:kr:ko:cars:vehicle:cruze",
				"ch:ich:kr:ko:cars:vehicle:malibu",
				"ch:ich:kr:ko:cars:vehicle:impala",
				"ch:ich:kr:ko:cars:rv:orlando",
				"ch:ich:kr:ko:cars:rv:captiva",
				"ch:ich:kr:ko:cars:rv:trax",
				"ch:ich:kr:ko:cars:rv:equinox",
				"ch:ich:kr:ko:cars:vehicle:aveo-sedan",
				"ch:ich:kr:ko:cars:vehicle:aveo-hatchback",
				"ch:ich:kr:ko:cars:vehicle:aveo-rs",
				"ch:ich:kr:ko:cars:sportscar:camaro",
				"ch:ich:kr:ko:event:",
				"ch:ich:kr:ko:mobile:event:"
		};
		//String param = "{ \"rsid\":\"gmapkrchevroletbb\", \"globalFilters\":[ { \"type\":\"dateRange\", \"dateRange\":\"2018-12-01T00:00:00.000/2018-12-02T00:00:00.000\" } ], \"search\":{ \"clause\": \"(CONTAINS 'ch:ich:kr:ko:cars:vehicle:spark') OR (CONTAINS 'ch:ich:kr:ko:cars:ev:volt') OR (CONTAINS 'ch:ich:kr:ko:cars:ev:boltev')\" }, \"metricContainer\":{ \"metrics\":[ { \"columnId\":\"0\", \"id\":\"metrics/pageviews\", \"filters\":[ \"0\" ] },\t{ \"columnId\":\"1\", \"id\":\"metrics/visits\", \"filters\":[ \"0\" ] } ], \"metricFilters\":[ { \"id\":\"0\", \"type\":\"dateRange\", \"dateRange\":\"2018-12-01T00:00:00.000/2018-12-02T00:00:00.000\" } ] }, \"dimension\":\"variables/page\", \"settings\":{ \"dimensionSort\":\"asc\", \"limit\":0, \"page\" : 0 }}";
		
		JsonParamService jsonParamService = new JsonParamService();
		String param = jsonParamService.buildJsonParam(chevoletReportSuiteIDs[0], "2018-12-02", chevoletKeywords);
		
		HttpPostService postService = new HttpPostService();
		HttpPost post = postService.buildHttpPost(postUrl, param);
		ResponseReport result = postService.executePost(post, ResponseReport.class);
		System.out.println(result);
		
	}
	
	

}
