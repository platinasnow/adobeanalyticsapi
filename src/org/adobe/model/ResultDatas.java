package org.adobe.model;

public class ResultDatas {
	private int pv;
	private int uv;
	private String TRFC;
	private String date;

	ResultDatas() {
	}

	public ResultDatas(int pv, int uv, String TRFC, String date) {
		this.pv = pv;
		this.uv = uv;
		this.TRFC = TRFC;
		this.date = date;
	}

	public int getPv() {
		return pv;
	}

	public void setPv(int pv) {
		this.pv = pv;
	}

	public int getUv() {
		return uv;
	}

	public void setUv(int uv) {
		this.uv = uv;
	}

	public String getTRFC() {
		return TRFC;
	}

	public void setTRFC(String tRFC) {
		TRFC = tRFC;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "ResultDatas [pv=" + pv + ", uv=" + uv + ", TRFC=" + TRFC + ", date=" + date + "]";
	}

}
