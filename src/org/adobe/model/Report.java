package org.adobe.model;

import java.util.Arrays;

public class Report {

	private String itemId;
	private String value;
	private int[] data;
	private int[] totals;

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int[] getData() {
		return data;
	}

	public void setData(int[] data) {
		this.data = data;
	}

	public int[] getTotals() {
		return totals;
	}

	public void setTotals(int[] totals) {
		this.totals = totals;
	}

	@Override
	public String toString() {
		return "Report [itemId=" + itemId + ", value=" + value + ", data=" + Arrays.toString(data) + ", totals="
				+ Arrays.toString(totals) + "]";
	}

}
