package org.adobe.model;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ResponseReport {

	private int totalPages;
	private boolean firstPage;
	private boolean lastPage;
	private int numberOfElements;
	private int number;
	private int totalElements;
	@JsonIgnore
	private String[] columns;
	private String[] columnIds;
	private List<Report> rows;
	private Report summaryData;

	public int getNumberOfElements() {
		return numberOfElements;
	}

	public void setNumberOfElements(int numberOfElements) {
		this.numberOfElements = numberOfElements;
	}

	public int getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}

	public List<Report> getRows() {
		return rows;
	}

	public void setRows(List<Report> rows) {
		this.rows = rows;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public boolean isFirstPage() {
		return firstPage;
	}

	public void setFirstPage(boolean firstPage) {
		this.firstPage = firstPage;
	}

	public boolean isLastPage() {
		return lastPage;
	}

	public void setLastPage(boolean lastPage) {
		this.lastPage = lastPage;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String[] getColumns() {
		return columns;
	}

	public void setColumns(String[] columns) {
		this.columns = columns;
	}

	public String[] getColumnIds() {
		return columnIds;
	}

	public void setColumnIds(String[] columnIds) {
		this.columnIds = columnIds;
	}

	public Report getSummaryData() {
		return summaryData;
	}

	public void setSummaryData(Report summaryData) {
		this.summaryData = summaryData;
	}

	@Override
	public String toString() {
		return "ResponseReport [totalPages=" + totalPages + ", firstPage=" + firstPage + ", lastPage=" + lastPage
				+ ", numberOfElements=" + numberOfElements + ", number=" + number + ", totalElements=" + totalElements
				+ ", columns=" + Arrays.toString(columns) + ", columnIds=" + Arrays.toString(columnIds) + ", rows="
				+ rows + ", summaryData=" + summaryData + "]";
	}
	
	

}
